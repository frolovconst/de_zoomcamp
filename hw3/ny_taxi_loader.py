import os
import pandas as pd

from pyarrow import Table
import pyarrow as pa
from pyarrow.fs import GcsFileSystem
from pyarrow import parquet as pq

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/work/edu/de_zoomcamp/turing-mark-412616-a7f85a028742.json"

def load_data(*args, **kwargs):
    """
    Template code for loading data from any source.

    Returns:
        Anything (e.g. data frame, dictionary, array, int, str, etc.)
    """
    # Specify your data loading logic here
    dfs = []
    for month in range(1, 13):
        url = f"https://d37ci6vzurychx.cloudfront.net/trip-data/green_tripdata_2022-{month:02d}.parquet"
        csv_name = os.path.basename(url)
        print(url)
        os.system(f"wget {url} -O ./data/{csv_name}")
        # print(pd.read_csv(csv_name).shape)
        # table = pq.read_table(f"./data/{csv_name}")
        dfs.append(pq.read_table(f"./data/{csv_name}"))
    df = pa.concat_tables(dfs)

    gcs = GcsFileSystem()
    bucket_name = 'zoomcamp-hw3'
    # object_key = 'ny_taxi.parquet'
    table_name = "ny_green_taxi_data"

    root_path = f"{bucket_name}/{table_name}"
    pq.write_to_dataset(
        df,
        root_path=root_path,
        # partition_cols=['lpep_pickup_date'],
        filesystem=gcs
    )

    # 'VendorID', 'lpep_pickup_datetime', 'lpep_dropoff_datetime',

    #    'store_and_fwd_flag', 'RatecodeID', 'PULocationID', 'DOLocationID',

    #    'passenger_count', 'trip_distance', 'fare_amount', 'extra', 'mta_tax',

    #    'tip_amount', 'tolls_amount', 'ehail_fee', 'improvement_surcharge',

    #    'total_amount', 'payment_type', 'trip_type', 'congestion_surcharge']

load_data()