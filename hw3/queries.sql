SELECT COUNT(*) FROM `turing-mark-412616.ny_green_taxi.ny_green_taxi`

SELECT DISTINCT(PULocationID) FROM `turing-mark-412616.ny_green_taxi.ny_green_taxi`

SELECT * FROM `turing-mark-412616.ny_green_taxi.ny_green_taxi_materialized` LIMIT 100


CREATE OR REPLACE EXTERNAL TABLE `turing-mark-412616.ny_green_taxi.ny_green_taxi_external`
OPTIONS (
  format = 'PARQUET',
  uris = ['gs://zoomcamp-hw3/ny_green_taxi_data/82b6a682c3514bc09f1fc5c5a80c0b3c-0.parquet']
);


CREATE OR REPLACE TABLE turing-mark-412616.ny_green_taxi.ny_green_taxi_non_partitoned AS
SELECT * FROM `ny_green_taxi.ny_green_taxi_external`

SELECT DISTINCT(PULocationID) FROM `turing-mark-412616.ny_green_taxi.ny_green_taxi_non_partitoned`

SELECT DISTINCT(PULocationID) FROM `turing-mark-412616.ny_green_taxi.ny_green_taxi_external`

SELECT COUNT(8) FROM `turing-mark-412616.ny_green_taxi.ny_green_taxi_non_partitoned`
WHERE fare_amount = 0

CREATE OR REPLACE TABLE `turing-mark-412616.ny_green_taxi.ny_green_taxi_partitoned`
PARTITION BY DATE(lpep_pickup_datetime)
CLUSTER BY PUlocationID AS (
  SELECT * FROM `turing-mark-412616.ny_green_taxi.ny_green_taxi_non_partitoned`
);

CREATE MATERIALIZED VIEW turing-mark-412616.ny_green_taxi.ny_green_taxi_materialized
AS (SELECT DISTINCT(PULocationID) FROM `turing-mark-412616.ny_green_taxi.ny_green_taxi` );