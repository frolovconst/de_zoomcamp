import os
import pandas as pd

if 'data_loader' not in globals():
    from mage_ai.data_preparation.decorators import data_loader
if 'test' not in globals():
    from mage_ai.data_preparation.decorators import test


@data_loader
def load_data(*args, **kwargs):
    """
    Template code for loading data from any source.

    Returns:
        Anything (e.g. data frame, dictionary, array, int, str, etc.)
    """
    # Specify your data loading logic here
    dfs = []
    for month in range(10, 13):
        url = f"https://github.com/DataTalksClub/nyc-tlc-data/releases/download/green/green_tripdata_2020-{month}.csv.gz"
        csv_name = f"green_tripdata_2020-{month}.csv.gz"
        os.system(f"wget {url} -O {csv_name}")
        print(pd.read_csv(csv_name).shape)
        dfs.append(pd.read_csv(csv_name))
    df = pd.concat(dfs, axis=0)

    # 'VendorID', 'lpep_pickup_datetime', 'lpep_dropoff_datetime',

    #    'store_and_fwd_flag', 'RatecodeID', 'PULocationID', 'DOLocationID',

    #    'passenger_count', 'trip_distance', 'fare_amount', 'extra', 'mta_tax',

    #    'tip_amount', 'tolls_amount', 'ehail_fee', 'improvement_surcharge',

    #    'total_amount', 'payment_type', 'trip_type', 'congestion_surcharge']

    print(df.columns)
    print(df.shape)

    return df


@test
def test_output(output, *args) -> None:
    """
    Template code for testing the output of the block.
    """
    assert output is not None, 'The output is undefined'
